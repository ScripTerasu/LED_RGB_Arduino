// Salida
int redPin = 9;   // LED rojo, conectado al pin digital 9
int grnPin = 10;  // LED verde, conectado al pin digital 10
int bluPin = 11;  // LED azul, conectado al pin digital 11

int redButtonPin = 2; // Boton todo rojo, conectado al pin digital 2
int yellowButtonPin = 3;  // Boton todo amarillo, conectado al pin digital 3
int fadingButtonPin = 4;  // Boton para desvanecimiento, conectado al pin digital 4

// Variables para leer el estado del pulsador
int redButtonState = 0;
int yellowButtonState = 0;
int fadingButtonState = 0;

// Arreglos de color
int black[3]  = { 0, 0, 0 };
int red[3]    = { 100, 0, 0 };
int orange[3] = { 100, 30, 0 };

int yellow[3] = { 100, 100, 0 };
int violet[3] = { 40, 0, 40 };
int pink[3] = { 78, 8, 52 };
int purple[3] = { 50, 0, 50 };
int transition[3] = { 100, 0, 40 };
int transition2[3] = { 80, 31, 31 };
int transition3[3] = { 80, 0, 31 };

// Establecer el color inicial
int redVal = black[0];
int grnVal = black[1];
int bluVal = black[2];

int wait = 5;      // Retraso interno del crossFade de 5ms; Aumento para desvanecimientos más lentos
int hold = 1000;       // Opción cuando un color está completo, antes del siguiente crossFade
int DEBUG = 1;      // DEBUG counter; Si se establece en 1, se escriben los valores de nuevo a través de serie
int loopCount = 60; // ¿Con qué frecuencia debería reportarse DEBUG?

// Inicializar variables de color
int prevR = redVal;
int prevG = grnVal;
int prevB = bluVal;

int cycleActive = 0;
int lastCycleState = cycleActive;

void setup()
{
  pinMode(redPin, OUTPUT);
  pinMode(grnPin, OUTPUT);
  pinMode(bluPin, OUTPUT);

  pinMode(redButtonPin, INPUT);
  pinMode(yellowButtonPin, INPUT);
  pinMode(fadingButtonPin, INPUT);

  if (DEBUG) {
    Serial.begin(9600);
  }
}

void loop()
{
  lastCycleState = cycleActive;
  if (cycleActive == 0) {
    crossFade(red);
    crossFade(orange);
    crossFade(violet);
    crossFade(pink);
  } else if (cycleActive == 1) {
    crossFade(red);
  }
  else if (cycleActive == 2) {
    crossFade(yellow);
  }
}

void changeCycle() {
  // Lea el estado del valor del pulsador:
  redButtonState = digitalRead(redButtonPin);
  yellowButtonState = digitalRead(yellowButtonPin);
  fadingButtonState = digitalRead(fadingButtonPin);
  // compruebe si se pulsa el pulsado.  Si lo es, el buttonState es LOW:
  if (redButtonState == LOW) {
    cycleActive = 1;
  }
  if (yellowButtonState == LOW) {
    cycleActive = 2;
  }
  if (fadingButtonState == LOW) {
    cycleActive = 0;
  }
}

int calculateStep(int prevValue, int endValue) {
  int step = endValue - prevValue; // ¿Cuál es la diferencia total?
  if (step) {                      // Si no es cero,
    step = 1020 / step;            //   dividir por 1020
  }
  return step;
}

int calculateVal(int step, int val, int i) {

  if ((step) && i % step == 0) { // Si step es diferente de cero y su tiempo para cambiar un valor,
    if (step > 0) {              //   Incrementar el valor si step es positivo ...
      val += 1;
    }
    else if (step < 0) {         //   ... o decrementarlo si el paso es negativo
      val -= 1;
    }
  }
  // Asegúrese de que val permanezca en el rango 0-255
  if (val > 255) {
    val = 255;
  }
  else if (val < 0) {
    val = 0;
  }
  return val;
}

/* CrossFade() convierte el porcentaje de colores en un
  0-255 rango, luego loops 1020 veces, comprobando para ver si
  El valor debe ser actualizado cada vez, luego escribir
  Los valores de color a los pines correctos.
*/
void crossFade(int color[3]) {
  // Convertir  a 0-255
  int R = (color[0] * 255) / 100;
  int G = (color[1] * 255) / 100;
  int B = (color[2] * 255) / 100;

  int stepR = calculateStep(prevR, R);
  int stepG = calculateStep(prevG, G);
  int stepB = calculateStep(prevB, B);

  for (int i = 0; i <= 1020; i++) {
    redVal = calculateVal(stepR, redVal, i);
    grnVal = calculateVal(stepG, grnVal, i);
    bluVal = calculateVal(stepB, bluVal, i);

    analogWrite(redPin, redVal);   // Escribir valores actuales a los pines LED
    analogWrite(grnPin, grnVal);
    analogWrite(bluPin, bluVal);

    changeCycle(); // Comprueba si se presiono algun boton
    if (lastCycleState != cycleActive) { // Si el ciclo cambia
      break;
    }
    delay(wait); // Pausa para 'wait' milisegundos antes de reanudar el ciclo


    if (DEBUG) { // If we want serial output, print it at the
      if (i == 0 or i % loopCount == 0) { // beginning, and every loopCount times
        Serial.print("Loop/RGB: #");
        Serial.print(i);
        Serial.print(" | ");
        Serial.print(redVal);
        Serial.print(" / ");
        Serial.print(grnVal);
        Serial.print(" / ");
        Serial.println(bluVal);
      }
      DEBUG += 1;
    }
  }
  // Actualizar los valores actuales para el siguiente bucle
  prevR = redVal;
  prevG = grnVal;
  prevB = bluVal;
  delay(hold); // Pausa para milisegundos opcionales 'wait' antes de reanudar el ciclo
}
